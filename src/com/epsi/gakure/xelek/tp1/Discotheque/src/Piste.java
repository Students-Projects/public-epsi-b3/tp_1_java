package com.epsi.gakure.xelek.tp1.Discotheque.src;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.epsi.gakure.xelek.tp1.Discotheque.organisation.PlayManager;


public class Piste extends Composition{
	private Album album;
	private String duration;
	private File file;
	private AudioInputStream audioInputStream;
	private long framesD;

	private double sDuration;




	public Piste(String name, Album album, String filePath)
	{
		super(name, album.getArtiste());
		this.album = album;
		this.file = new File(filePath);

		try {
			this.audioInputStream = AudioSystem.getAudioInputStream(this.file);

			AudioFormat format = audioInputStream.getFormat();

			long frames = audioInputStream.getFrameLength();
			double d = (frames+0.0) / format.getFrameRate(); 

			this.duration = super.ConvertDurationToString(d);
			this.sDuration = d;
			this.framesD=frames;


		} catch (UnsupportedAudioFileException e) {
			System.out.println(super.getname()+" : Format de fichier non supporté ... ");
		} catch (IOException e) {
			System.out.println(super.getname()+" : Impossible de charger ce fichier");
		}

		album.addPiste(this);
	}

	public Album getAlbum() {
		return album;
	}

	public String getName() {
		return super.getname();
	}


	@Override
	public String getDuration()
	{
		return this.duration;
	}

	public double getsDuration()
	{
		return this.sDuration;
	}

	public long getFrameDuration()
	{
		return this.framesD;
	}

	public AudioInputStream getAudioInputStream()
	{
		return this.audioInputStream;
	}	
	
	public void jouer(PlayManager pM)
	{
		pM.addToReadList(this);
	}
}
