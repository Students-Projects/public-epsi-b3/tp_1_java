package com.epsi.gakure.xelek.tp1.Discotheque.src;

import java.util.ArrayList;

import com.epsi.gakure.xelek.tp1.Discotheque.organisation.PlayManager;

public class Discotheque {
	private ArrayList<Album> albums = new ArrayList<>();
	private PlayManager pM = new PlayManager();
	
	public Discotheque(Album...myAlbums)
	{
		for (Album a:myAlbums)
		{
			this.albums.add(a);
		}
	}
	
	public Discotheque(ArrayList<Album> myAlbums)
	{
		for (Album a:myAlbums)
		{
			this.albums.add(a);
		}
	}
	
	
	public void addAlbum(Album... myAlbums)
	{
		for (Album a:myAlbums)
		{
			this.albums.add(a);
		}
	}

	public ArrayList<Album> getAlbums() {
		return this.albums;
	}
	
	public Album getAlbum(int i)
	{
		return this.albums.get(i);
	}
	
	public void listAlbums()
	{
		int i = 0;
		for (Album a:this.albums)
		{
			System.out.println(++i+". \t"+a.getName());
		}
	}
	
	public void listArtiste()
	{
		int i = 0;
		ArrayList<Artiste> Arts = new ArrayList<>();
		for (Album a:this.albums)
		{
			if (!Arts.contains(a.getArtiste()))
			{
				Arts.add(a.getArtiste());
			}
		}
		
		for (Artiste a: Arts)
		{
			System.out.println(++i+". \t"+a.getNom());
		}	
	}
	
	public ArrayList<Artiste> getArtistes()
	{
		int i =0;
		ArrayList<Artiste> arts = new ArrayList<>();
		for (Album a:this.albums)
		{
			if (!arts.contains(a.getArtiste()))
				arts.add(a.getArtiste());
		}
		return arts;
	}
	
	public ArrayList<Album> getAlbumByName(String name)
	{
		ArrayList<Album> research = new ArrayList<>();
		for (Album a: albums)
		{
			if (a.getName().toLowerCase().contains(name.toLowerCase()))
			{
				research.add(a);
			}
		}
		return research;
	}
	
	public ArrayList<Album> getAlbumByDate(int annee)
	{
		ArrayList<Album> research = new ArrayList<>();
		for (Album a: albums)
		{
			if (a.getAnneeParution() == annee)
			{
				research.add(a);
			}
		}
		return research;
	}
	
	public ArrayList<Album> getAlbumByArtiste(String Aname)
	{
		ArrayList<Album> research = new ArrayList<>();
		for (Album a: albums)
		{
			if (a.getArtiste().getNom().toLowerCase().contains(Aname.toLowerCase()))
			{
				research.add(a);
			}
		}
		return research;
	}
	
	public PlayManager getPlayerManager()
	{
		return this.pM;
	}
 
}
