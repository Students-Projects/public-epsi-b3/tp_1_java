package com.epsi.gakure.xelek.tp1.Discotheque.src;

import java.util.ArrayList;
import java.util.Date;

public class Album extends Composition{
	
	private String name;
	private ArrayList<Piste> pistes = new ArrayList<>();
	
	private int anneeParution;
	
	public Album(String name, Artiste artiste)
	{
		super(name, artiste);
		artiste.addAlbums(this);
	}
	
	public Album(String name, Artiste artiste, int anneeParution)
	{
		super(name, artiste);
		this.anneeParution = anneeParution;
		artiste.addAlbums(this);
	}
	
	
	
	public int getAnneeParution() {
		return anneeParution;
	}
	
	@Override
	public String getDuration()
	{
		double d = 0;
		for (Piste p: this.pistes)
		{
			d+=p.getsDuration();
		}
		
		return super.ConvertDurationToString(d);
	}



	public Artiste getArtiste() {
		return super.getArtiste();
	}

	public String getName() {
		return super.getname();
	}


	public ArrayList<Piste> getPistes() {
		return pistes;
	}
	
	public void addPiste(Piste piste)
	{
		this.pistes.add(piste);
	}
	
	public void listPistes()
	{
		int i = 0;
		for (Piste p:this.pistes)
		{
			System.out.println(++i+". \t"+p.getName()+"\t"+p.getDuration() );
		}
	}
	
	
	

}
