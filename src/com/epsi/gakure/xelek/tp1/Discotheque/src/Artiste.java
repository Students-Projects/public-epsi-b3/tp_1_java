package com.epsi.gakure.xelek.tp1.Discotheque.src;

import java.util.ArrayList;

public class Artiste {

	private String nom;
	private ArrayList<Album> albums = new ArrayList<>();
	
	
	public Artiste(String nom)
	{
		this.nom = nom;
	}
	
	public void addAlbums(Album...albums)
	{
		for (Album a: albums)
		if(!this.albums.contains(a))
			this.albums.add(a);
	}
	
	public void listAlbums()
	{
		int i = 0;
		for (Album a:this.albums)
		{
			System.out.println(++i+". \t"+a.getName());
		}
	}
	
	public ArrayList<Album> getAlbums()
	{
		return this.albums;
	}
	
	public String getNom()
	{
		return this.nom;
	}
	
	public void clearData()
	{
		this.albums.clear();
	}
}
