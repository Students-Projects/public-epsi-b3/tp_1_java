package com.epsi.gakure.xelek.tp1.Discotheque.src;

public abstract class Composition {
	
	private final String name;
	private final Artiste artiste;
	
	public Composition(String name, Artiste artiste)
	{
		this.name = name;
		this.artiste = artiste;
	}
	
	public abstract String getDuration();
	
	public String getname() {
		return name;
	}
	
	public Artiste getArtiste()
	{
		return artiste;
	}
	
	public String ConvertDurationToString(double d)
	{
		int minutes = (int) ((d) / 60);
		int seconds = (int) (d % 60);
		
		if (minutes >= 10 && seconds >= 10)
			return minutes + ":" + seconds; 
		
		String M = ""+minutes;
		String S = ""+seconds;
		
		if (minutes < 10)
		{
			M = "0"+minutes;
		}
		if (seconds<10)
		{
			S = "0"+seconds;
		}
		return M + ":" + S;
	}

}
