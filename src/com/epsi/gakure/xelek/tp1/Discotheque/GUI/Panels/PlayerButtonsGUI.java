package com.epsi.gakure.xelek.tp1.Discotheque.GUI.Panels;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;
import com.epsi.gakure.xelek.tp1.Discotheque.organisation.PlayManager;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Album;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Piste;

public class PlayerButtonsGUI extends JPanel implements ActionListener, MouseListener{
	
	
	Map<JButton, String> playerbuttons = new HashMap<>();
	PlayManager PM;
	boolean playing;
	JButton START_STOP_button;
	
	//MAP<String, > // <button_id, <button_type, button_path>> 
	
	
	public PlayerButtonsGUI(GenerateData data)
	{
		
		this.PM = data.getD().getPlayerManager();
		this.playing = false;
		
		JPanel buttons = new JPanel();
		
		this.add(CreatePlayerButton("src/static/Images/PlayerButtons/Previous.png", "Previous"));
		this.START_STOP_button = CreatePlayerButton("static/Images/PlayerButtons/Play.png", "Play/Pause");
		this.add(START_STOP_button);
		this.add(CreatePlayerButton("static/Images/PlayerButtons/Stop.png", "Stop"));
		this.add(CreatePlayerButton("static/Images/PlayerButtons/NEXT.png", "Next"));
		
		this.add(buttons);
	}
	
	private JButton CreatePlayerButton(String imagePath, String Title)
	{
		BufferedImage buttonIcon;
		JButton NextButton;
		try {
			
			buttonIcon = ImageIO.read(new File(imagePath));
			NextButton = new JButton(new ImageIcon(buttonIcon));
			NextButton.setBorder(BorderFactory.createEmptyBorder());
			NextButton.setContentAreaFilled(false);
			NextButton.setPreferredSize(new Dimension(64, 64));			
		} catch (IOException e) {
			NextButton = new JButton(Title);
		}
		
		NextButton.addActionListener(this);
		NextButton.addMouseListener(this);
		playerbuttons.put(NextButton, Title);
		return NextButton;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (this.playerbuttons.containsKey(e.getSource())) 
		{
			JButton bruh = (JButton) e.getSource();
			
			switch (this.playerbuttons.get(e.getSource())) {
			case "Previous":
				break;
			case "Play/Pause":
				if (this.PM.getCLip().isActive())
				{
					PM.pause();
					this.playing = false;
				}
				else if (this.PM.getCLip().isOpen())
				{
					this.playing = true;
					PM.jouer();
				}
				break;
			case "Next":
				break;
			case "Stop":
				this.START_STOP_button.setIcon(new ImageIcon("static/Images/PlayerButtons/Play.png"));
				this.PM.arreter();
				break;
				
			}
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (this.playerbuttons.containsKey(e.getSource())) 
		{
			JButton bruh = (JButton) e.getSource();
			
			switch (this.playerbuttons.get(bruh)) {
			case "Previous":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Hover/Previous.png"));
				break;
			case "Play/Pause":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Hover/Play.png"));
				break;
			case "Next":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Hover/NEXT.png"));
				break;
			case "Stop":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Hover/Stop.png"));
				break;
			}
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (this.playerbuttons.containsKey(e.getSource())) 
		{
			JButton bruh = (JButton) e.getSource();
			
			switch (this.playerbuttons.get(bruh)) {
			case "Previous":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Previous.png"));
				break;
			case "Play/Pause":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Play.png"));
				break;
			case "Next":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/NEXT.png"));
				break;
			case "Stop":
				bruh.setIcon(new ImageIcon("static/Images/PlayerButtons/Stop.png"));
				break;
			}
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (this.playerbuttons.containsKey(e.getSource())) 
		{
			JButton bruh = (JButton) e.getSource();
		}
	}
	
	private void changerStartPause()
	{
		
	}
	
}
