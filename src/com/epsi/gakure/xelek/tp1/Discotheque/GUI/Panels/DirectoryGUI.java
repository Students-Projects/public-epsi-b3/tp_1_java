package com.epsi.gakure.xelek.tp1.Discotheque.GUI.Panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Album;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Artiste;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Piste;

public class DirectoryGUI extends JPanel implements ActionListener {

	private Map<JButton, Artiste> artistes_buttons = new HashMap<>();
	private Map<Artiste, JPanel> album_menu = new HashMap<>();
	private Map<JButton, Album> album_button = new HashMap<>();
	private Map<Album, JPanel> piste_menu = new HashMap<>();
	private Map<JButton, Piste> piste_button = new HashMap<>();

	public DirectoryGUI(GenerateData data) {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		for (Artiste a : data.getArtistes()) {
			this.artistes_buttons.put(new JButton(a.getNom()), a);
			this.album_menu.put(a, new JPanel());
			this.album_menu.get(a).setLayout(new BoxLayout(this.album_menu.get(a), BoxLayout.PAGE_AXIS));

			for (Album alb : a.getAlbums()) {
				this.album_button.put(new JButton(alb.getname()), alb);
				this.piste_menu.put(alb, new JPanel());
				this.piste_menu.get(alb).setLayout(new BoxLayout(this.piste_menu.get(alb), BoxLayout.PAGE_AXIS));

				for (Piste p : alb.getPistes()) {
					this.piste_button.put(new JButton(p.getname()), p);
				}
				this.piste_menu.get(alb).setVisible(false);
				Set<Entry<JButton, Piste>> set_pst_btn = piste_button.entrySet();
				Iterator<Entry<JButton, Piste>> pst_it = set_pst_btn.iterator();

				while (pst_it.hasNext()) {
					Entry<JButton, Piste> e_pst = pst_it.next();
					e_pst.getKey().addActionListener(this);
					this.piste_menu.get(alb).add(e_pst.getKey());
				}

			}
			this.album_menu.get(a).setVisible(false);

			Set<Entry<JButton, Album>> set_alb_btn = album_button.entrySet();

			Iterator<Entry<JButton, Album>> alb_it = set_alb_btn.iterator();

			while (alb_it.hasNext()) {

				Entry<JButton, Album> e_alb = alb_it.next();
				e_alb.getKey().addActionListener(this);
				this.album_menu.get(a).add(e_alb.getKey());
				this.album_menu.get(a).add(this.piste_menu.get(e_alb.getValue()));

			}

		}

		Set<Entry<JButton, Artiste>> setHm = artistes_buttons.entrySet();

		Iterator<Entry<JButton, Artiste>> it = setHm.iterator();

		while (it.hasNext()) {

			Entry<JButton, Artiste> e = it.next();
			e.getKey().addActionListener(this);
			this.add(e.getKey());
			this.add(this.album_menu.get(e.getValue()));

		}
	}

	private void collapse(JPanel jp) {
		if (jp.isVisible())
			jp.setVisible(false);
		else
			jp.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (this.artistes_buttons.containsKey(e.getSource())) {
			this.collapse(this.album_menu.get(this.artistes_buttons.get(e.getSource())));
		}

		if (this.album_button.containsKey(e.getSource())) {
			this.collapse(this.piste_menu.get(this.album_button.get(e.getSource())));
		}
	}

}
