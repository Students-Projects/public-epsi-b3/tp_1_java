package com.epsi.gakure.xelek.tp1.Discotheque.GUI.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;

public class LeftPanel extends JPanel {
	public LeftPanel(GenerateData data)
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.BLUE);
		
		PlayerGUI PG = new PlayerGUI();
		PG.setSize(new Dimension(this.getWidth(), 2*this.getHeight()/3));
		
		JPanel PlayerScreen = new JPanel();
		PlayerScreen.setPreferredSize(new Dimension(720,480));
		
		PG.add(PlayerScreen);
		
		
		PlayerButtonsGUI pgb = new PlayerButtonsGUI(data);
		pgb.setSize(new Dimension(this.getWidth(), this.getHeight()/3));
		
		this.add(PG, BorderLayout.NORTH);
		this.add(pgb, BorderLayout.SOUTH);
		
	}

}
