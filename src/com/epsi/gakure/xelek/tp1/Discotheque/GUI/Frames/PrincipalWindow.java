package com.epsi.gakure.xelek.tp1.Discotheque.GUI.Frames;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.epsi.gakure.xelek.tp1.Discotheque.GUI.Panels.LeftPanel;
import com.epsi.gakure.xelek.tp1.Discotheque.GUI.Panels.RightPanel;
import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;

public class PrincipalWindow extends JFrame {
	
	private String title = "Discothèque";
	private String Submenu = "";
	
	public PrincipalWindow(GenerateData data)
	{
		this.refreshTitle();
		this.setSize(1280, 720);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setResizable(false);
		//this.isResizable();
		
		JPanel PrP = new JPanel();
		LeftPanel LP = new LeftPanel(data);
		LP.setSize(new Dimension(3*this.getWidth()/4, this.getHeight()));		
		LP.setPreferredSize(new Dimension(3*this.getWidth()/4 - 10, this.getHeight()-45));
		
		RightPanel RP = new RightPanel(data);
		RP.setSize(new Dimension(this.getWidth()/4, this.getHeight()));
		RP.setPreferredSize(new Dimension(this.getWidth()/4 - 10, this.getHeight()-45));
		
		PrP.setSize(this.getWidth(), this.getHeight());
		
		//LP.setPreferredSize(new Dimension();
		//RP.setPreferredSize(new Dimension();
		
		
		PrP.add(LP);
		PrP.add(RP);
		this.setContentPane(PrP);
	}
	
	
	
	public String getTitle()
	{
		return this.title;
	}
	
	public void setSubMenu(String subM)
	{
		this.Submenu = subM;
		this.refreshTitle();
	}
	
	private void refreshTitle()
	{
		if (this.Submenu.isEmpty())
			this.setTitle(this.title);
		else
			this.setTitle(this.title + " - " + this.Submenu);
	}

}
