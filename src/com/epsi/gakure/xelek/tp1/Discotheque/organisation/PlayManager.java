package com.epsi.gakure.xelek.tp1.Discotheque.organisation;

import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Piste;

public class PlayManager {
	private Clip clip;
	private ArrayList<Piste> pistes = new ArrayList<>();
	
	private long SavedclipTime = 0;

	public PlayManager() {
		try {
			this.clip = AudioSystem.getClip();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void PlayerMenu(GenerateData data) {
		System.out.println("Menu de Lecture");

	}

	public void addToReadList(Piste P) {
		if (!this.pistes.contains(P)) {
			this.pistes.add(P);
			this.changeMusic(P);

		}
	}

	public void changeMusic(Piste p, Piste actualPiste) {
		if (!p.equals(actualPiste)) {
			if (this.clip.isActive()) {
				this.clip.stop();
				this.clip.close();
			}
			try {
				this.clip.open(p.getAudioInputStream());
				System.out.println("\n" + "Playing " + p.getname());
				this.jouer();

			} catch (LineUnavailableException e) {
				System.out.println(p.getname() + " : Impossible de lancer votre musique");

			} catch (IOException e) {
				System.out.println(p.getname() + " : Impossible de charger ce fichier");
			}
		}
	}

	public void changeMusic(Piste p) {
		if (this.clip.isActive()) {
			this.clip.stop();
			this.clip.close();
		}
		try {
			this.clip.open(p.getAudioInputStream());
			System.out.println("\n" + "Playing " + p.getname());
			this.jouer();

		} catch (LineUnavailableException e) {
			System.out.println(p.getname() + " : Impossible de lancer votre musique");

		} catch (IOException e) {
			System.out.println(p.getname() + " : Impossible de charger ce fichier");
		}
	}

	public void jouer() {
		this.clip.setMicrosecondPosition(SavedclipTime);
		this.clip.start();
	}

	public void pause() {
		this.SavedclipTime = clip.getMicrosecondPosition();
		this.clip.stop();
	}

	public void arreter() {
		this.clip.stop();
		this.clip.close();
		clearReadlist();
		this.SavedclipTime = 0;
	}

	public void clearReadlist() {
		this.pistes.clear();
	}
	
	public Clip getCLip()
	{
		return this.clip;
	}
}
