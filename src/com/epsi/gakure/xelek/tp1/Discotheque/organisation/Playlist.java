package com.epsi.gakure.xelek.tp1.Discotheque.organisation;

import java.util.ArrayList;

import com.epsi.gakure.xelek.tp1.Discotheque.src.Piste;

public class Playlist {
	
	private String name;
	private ArrayList<Piste> pistes = new ArrayList<>(); 
	
	public Playlist(String name, Piste...pistes)
	{
		this.name = name;
		for (Piste p:pistes)
			this.pistes.add(p);
	}
	
	public Playlist(String name)
	{
		this.name = name;
	}
	
	
	public void LaunchPlaylist(PlayManager pM)
	{
		for (Piste p:this.pistes)
		{
			p.jouer(pM);
		}
	}
	
	
	public void addPiste(Piste...p)
	{
		boolean canIadd = true;
		
		for (Piste P:p)
		{
			if (!pistes.contains(P))
			{
				this.pistes.add(P);
			}
		}
	}
}
