package com.epsi.gakure.xelek.tp1.Discotheque.menus;

import java.util.ArrayList;
import java.util.Scanner;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;
import com.epsi.gakure.xelek.tp1.Discotheque.organisation.PlayManager;
import com.epsi.gakure.xelek.tp1.Discotheque.search.SearchAlbum;
import com.epsi.gakure.xelek.tp1.Discotheque.search.SearchPiste;
import com.epsi.gakure.xelek.tp1.Discotheque.search.searchManager;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Album;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Artiste;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Discotheque;

public class Menu {
	public static void Accueil(GenerateData data)
	{
		
		Scanner optionMenu = new Scanner(System.in);
		
		System.out.println("Que voulez-vous faire ?" + "\n");
		System.out.println("\t" + "1. Lister les albums");
		System.out.println("\t" + "2. Lister les artistes");
		System.out.println("\t" + "3. Rechercher des albums");
		System.out.println("-------------------------------");
		System.out.println("\t"+ "4. ReadList");
		System.out.println("\t" + "5. Ajouter Data");
		System.out.println("\t" + "6. Actualiser Data");
		System.out.println("-------------------------------");
		System.out.println("\t" + "7. Play");
		System.out.println("\t" + "8. Pause");
		System.out.println("\t" + "9. Stop");
		System.out.println("\t" + "0. Quitter l'application");
		System.out.print("\n" + "Votre choix ?  ");
		try {
			int option = optionMenu.nextInt();
			switch (option) {
			case 0:
				System.out.println("\n" + "Merci, à la prochaine :3 ");
				System.exit(0);
			case 1:
				menuDiscothequeAlbums(data);
				break;
			case 2:
				menuArtiste(data);
				break;
			case 3:
				searchManager.searchType(data);
				break;
			case 4:
				//PlayManager.PlayerMenu(data);
				System.out.println("\n" + "Option Momentanément indisponible");
				Accueil(data);
				break;
			case 5:
				//data.addData();
				System.out.println("\n" + "Option Momentanément indisponible");
				Accueil(data);
				break;
			case 6:
				data.refreshData();
				break;
			case 7:
				data.getD().getPlayerManager().jouer();
				Accueil(data);
				break;
			case 8:
				data.getD().getPlayerManager().pause();
				Accueil(data);
				break;
			case 9:
				data.getD().getPlayerManager().arreter();
				Accueil(data);
				break;
			default:
				System.out.println("\n" + "Cette option n'est pas disponible u_u " + "\n");
				Accueil(data);
			}		

		} catch (Exception e) {
			optionMenu.close();
			System.out.println("\n" + "Chut, c'est pas bon °3° !!!" + "\n");
			Accueil(data);
		}
	}

	private static void menuDiscothequeAlbums(GenerateData data)
	{
		System.out.println("\n" + "Liste des albums" + "\n");

		data.getD().listAlbums();

		Scanner mDaOptionMenu = new Scanner(System.in);

		System.out.print("\n" + "Votre sélection (0 pour retourner à l'accueil) ?  ");
		try {
			int option = mDaOptionMenu.nextInt();

			if (option == 0)
			{
				Accueil(data);
			}
			else if (option > 0 && option <= data.getD().getAlbums().size() )
			{
				menuAlbum(data, data.getD().getAlbum(option-1));
			}			
			else
			{
				System.out.println("\n" + "Argh, cet album n'existe pas !!!");
				menuDiscothequeAlbums(data);
			}
		} catch (Exception e) {
			System.out.println("\n" + "blblblb, pas ça °3° !!!");
			menuDiscothequeAlbums(data);
		}		
	}

	public static void menuAlbum(GenerateData data, Album album)
	{
		System.out.println("\n" + "Titre : " + album.getName());
		System.out.println("Durée : " + album.getDuration());
		System.out.println("Artiste : " + album.getArtiste().getNom());
		System.out.println("\n");
		album.listPistes();

		Scanner optionMenu = new Scanner(System.in);

		System.out.print("Numéro de la piste à jouer (0 pour retourner à l'accueil) :");

		try 
		{
			int option = optionMenu.nextInt();
			if (option == 0)
			{
				Accueil(data);
			}
			else if (option > 0 && option <= album.getPistes().size())
			{
				album.getPistes().get(option-1).jouer(data.getD().getPlayerManager());
				Accueil(data);
			}
			else
			{
				System.out.println("\n" + "Désolé, cette option n'est pas disponible u_u " + "\n");
				menuAlbum(data, album);
			}
		}
		catch (Exception e) {
			System.out.println("\n" + "Chut, c'est pas bon °3° !!!");
			menuDiscothequeAlbums(data);
		}
	}

	private static void menuArtiste(GenerateData data)
	{
		System.out.println("\n" + "Liste des artistes" + "\n");
		
		Scanner optionMenu = new Scanner(System.in);

		data.getD().listArtiste();

		try
		{
			System.out.print("\n" + "Votre sélection (0 pour retourner à l'accueil) ?  ");
			int option = optionMenu.nextInt();
			if (option == 0)
			{
				Accueil(data);
			}
			else if (option > 0 && option <= data.getD().getArtistes().size())
			{
				menuAlbumArtiste(data, data.getD().getArtistes().get(option-1));
			}
			else 
			{
				menuArtiste(data);
			}
		}
		catch (Exception e) {
			System.out.println("\n" + "Aïe, essaie de rentrer autre chose °3° !!!");
			menuArtiste(data);
		}
	}
	
	private static void menuAlbumArtiste(GenerateData data, Artiste a)
	{
		System.out.println("\n" + "Liste des albums de l'artiste " + a.getNom() + "\n");
		Scanner optionMenu = new Scanner(System.in);
		
		a.listAlbums();
		
		try {
			
			System.out.print("\n" + "Votre sélection (0 pour retourner à l'accueil) ?  ");
			int option = optionMenu.nextInt();

			if (option == 0)
			{
				Accueil(data);
			}
			else if (option > 0 && option <= a.getAlbums().size() )
			{
				menuAlbum(data, a.getAlbums().get(option-1));
			}			
			else
			{
				System.out.println("\n" + "Argh, cet album n'existe pas !!!");
				menuAlbumArtiste(data, a);
			}
		} catch (Exception e) {
			System.out.println("\n" + "Chut, c'est pas bon °3° !!!");
			menuAlbumArtiste(data, a);
		}
		
	}
	
}
