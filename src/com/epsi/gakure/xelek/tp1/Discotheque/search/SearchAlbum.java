package com.epsi.gakure.xelek.tp1.Discotheque.search;

import java.util.ArrayList;
import java.util.Scanner;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;
import com.epsi.gakure.xelek.tp1.Discotheque.menus.Menu;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Album;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Discotheque;

public class SearchAlbum {

	public static void searchAlbumByElement(GenerateData data, int facteurRecherche) {
		System.out.print("Recherche par ");
		Scanner optionMenu = new Scanner(System.in);
		String option;
		int anneeOption;

		switch (facteurRecherche) {
		case 1:
			System.out.println("nom d'artiste");
			option = optionMenu.nextLine();
			AlbumSearchResults(data.getD().getAlbumByArtiste(option), option, data);
			break;
		case 2:
			System.out.println("titre d'album");
			option = optionMenu.nextLine();
			AlbumSearchResults(data.getD().getAlbumByName(option), option, data);
			break;
		case 3:
			System.out.println("année d'album");
			anneeOption = optionMenu.nextInt();
			AlbumSearchResults(data.getD().getAlbumByDate(anneeOption), Integer.toString(anneeOption), data);
			break;
		default:
			System.out.println("\n" + "Cette option de recherche n'est pas disponible u_u " + "\n");
			searchManager.searchType(data);
			break;
		}

	}

	public static void AlbumSearchResults(ArrayList<Album> results, String search, GenerateData data) {
		Scanner optionMenu = new Scanner(System.in);
		int option;

		System.out.println("Résultat pour \"" + search + "\" : " + "\n");
		if (results.size() == 0) {
			System.out.println("Aucun Résulat" + "\n");
		} else {
			int i = 0;
			for (Album r : results) {
				System.out.println(++i + ". " + r.getName());
			}
		}
		System.out.print("\n" + "Votre sélection (0 pour retourner à l'accueil) ? ");
		try {
			option = optionMenu.nextInt();
			if (option > 0 && option <= results.size()) {
				Menu.menuAlbum(data, results.get(option - 1));
			} else if (option == 0) {
				Menu.Accueil(data);
			}
		} catch (Exception e) {
			System.out.println("\n" + "Cette option de recherche n'est pas disponible u_u " + "\n");
			AlbumSearchResults(results, search, data);
		}

	}
}
