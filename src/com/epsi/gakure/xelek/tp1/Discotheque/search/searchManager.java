package com.epsi.gakure.xelek.tp1.Discotheque.search;

import java.util.Scanner;

import com.epsi.gakure.xelek.tp1.Discotheque.data.GenerateData;
import com.epsi.gakure.xelek.tp1.Discotheque.menus.Menu;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Discotheque;

public class searchManager {
	
	public static void searchType(GenerateData data)
	{
		SearchAlbum sA = new SearchAlbum();
		SearchPiste sP = new SearchPiste();
		
		Scanner optionMenu = new Scanner(System.in);
		
		System.out.println("\n" + "Quel type de recherche souhaitez-vous ?" + "\n");
		System.out.println("\t" + "1. par nom d'artiste");
		System.out.println("\t" + "2. par titre d'album");
		System.out.println("\t" + "3. par année d'album");
		System.out.print("\n" + "Votre sélection (0 pour retourner à l'accueil) ? ");
		try {
			int option = optionMenu.nextInt();
			if (option == 0)
			{
				Menu.Accueil(data);
			}
			else if (option > 3)
			{
				System.out.println("\n" + "Cette option de recherche n'est pas disponible u_u " + "\n");
				searchType(data);
			}
			else
			{
				sA.searchAlbumByElement(data, option);
			}
		} catch (Exception e) {
			System.out.println("\n" + "Chut, c'est pas bon °3° !!!" + "\n");
			searchType(data);
		}
	}

}
