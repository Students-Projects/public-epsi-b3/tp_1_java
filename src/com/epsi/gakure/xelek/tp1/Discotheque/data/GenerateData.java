package com.epsi.gakure.xelek.tp1.Discotheque.data;

import java.io.File;
import java.util.ArrayList;

import javax.sound.sampled.Clip;

import com.epsi.gakure.xelek.tp1.Discotheque.src.Album;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Artiste;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Discotheque;
import com.epsi.gakure.xelek.tp1.Discotheque.src.Piste;

public class GenerateData {

	private Discotheque d;

	private ArrayList<Album> Albums = new ArrayList<>();
	private ArrayList<Piste> Pistes = new ArrayList<>();
	private ArrayList<Artiste> Artistes = new ArrayList<>();

	private Artiste noArtiste = new Artiste("No Artist");
	private Album noAlbumNoArtist = new Album("No Album", noArtiste);

	private String MusicDirectory = "Musics";

	public void DataGen() {

		refreshData();
		this.Albums.add(noAlbumNoArtist);
		this.d = new Discotheque(this.Albums);
	}

	/*
	 * Search into Music directory all elements.
	 * 	- Add artist if the analised element is a directory 'isDirectory() -> true'
	 *  - Add Pist without artist and album is it's a File  'isDirectory() -> false'
	 */
	private void getArtisteDirs() {
		File f = new File(this.MusicDirectory);

		for (String i : f.list()) {
			String analisedFilePath = this.MusicDirectory + "/" + i;
			File artist = new File(analisedFilePath);
			if (artist.isDirectory()) {
				Artiste newArtist = new Artiste(artist.getName());
				Album nA = new Album(artist.getName() + " - No Album", newArtist);
				this.Albums.add(nA);
				if (!this.Artistes.contains(newArtist)) {
					this.Artistes.add(newArtist);
				}
				getAlbumDirs(analisedFilePath, newArtist, nA);
			} else {
				this.Pistes.add(new Piste(i, noAlbumNoArtist, analisedFilePath));
			}
		}
	}

	private void getAlbumDirs(String ArtistePath, Artiste a, Album nA) {
		File f = new File(ArtistePath);

		for (String i : f.list()) {
			String analisedFilePath = ArtistePath + "/" + i;
			File album = new File(analisedFilePath);

			if (album.isDirectory()) {
				Album newAblum = new Album(album.getName(), a);
				if (!this.Albums.contains(newAblum)) {
					this.Albums.add(newAblum);
				}
				getPistesFiles(analisedFilePath, newAblum);
			} else {
				this.Pistes.add(new Piste(i, nA, analisedFilePath));
			}

		}

	}

	private void getPistesFiles(String AlbumPath, Album alb) {
		File f = new File(AlbumPath);

		for (String i : f.list()) {
			String filePath = AlbumPath + "/" + i;
			File piste = new File(filePath);

			if (piste.isFile()) {
				Piste newPiste = new Piste(piste.getName(), alb, filePath);
				if (!this.Pistes.contains(newPiste)) {
					this.Pistes.add(newPiste);
				}
			}
		}
	}

	public void refreshData() {
		clearData();
		getArtisteDirs();
	}

	public void addData() {

	}

	private void clearData() {
		this.Artistes.clear();
		this.Albums.clear();
		this.Pistes.clear();
	}

	public Discotheque getD() {
		return d;
	}

	public ArrayList<Album> getAlbums() {
		return Albums;
	}

	public ArrayList<Piste> getPistes() {
		return Pistes;
	}

	public ArrayList<Artiste> getArtistes() {
		return Artistes;
	}

	
	
}
