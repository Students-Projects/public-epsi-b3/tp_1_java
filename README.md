**Music Player**
------------------

<u>Projet Java Epsi</u>


_____________________________
__Format de lecture du dossier "Musics"__


> Musics
>> Dossier d'artiste
>>> Dossier d'album
>>>> Fichier de musique

- Une musique ne se trouvant pas dans un dossier d'album sera considérée comme sans album.
- Une musique ne se trouvant pas dans le dossier d'artiste sera considérée comme sans album ni artiste
- ATTENTION : Un dossier d'album dans le dossier "Musics" sera considéré comme un dossier d'artiste.

______________________________
__Format de musique__

Ce programme n'est actuellement capable que de lire des musique au format wave

_____________________________

__But actuels__

- Réaliser l'interface graphique
	Afin de voir l'avancement, de la partie graphique, mettez la variable "graphical" en ``true`` dans run.java
______________________________
Release 0.1 - Lauret Julien - EPSI B3 Groupe 2
